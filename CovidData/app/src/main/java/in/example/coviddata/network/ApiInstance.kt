package `in`.example.coviddata.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiInstance {

    companion object {

        var logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        var okHttpClient = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS).addInterceptor(logging)
            .build()


        fun getApiInstance(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://data.covid19india.org/")
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build()
        }
    }

}