package `in`.example.coviddata.network

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager

class NetworkStatus {

    companion object{

        fun networkAvailable(app: Application): Boolean{
            val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo?.isConnectedOrConnecting ?: false
        }
    }
}