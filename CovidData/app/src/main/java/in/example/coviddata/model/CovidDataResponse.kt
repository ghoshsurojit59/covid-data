package `in`.example.coviddata.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class CovidDataResponse(

	@PrimaryKey(autoGenerate = true)
	val id: Int?,

	@Expose
	@field:SerializedName("cases_time_series")
	val casesTimeSeries: List<CasesTimeSeriesItem?>?,

	@Expose
	@field:SerializedName("tested")
	val tested: List<TestedItem?>?,

	@Expose
	@field:SerializedName("statewise")
	val statewise: List<StatewiseItem?>?
)

@Entity
data class StatewiseItem(

	@Expose
	@field:SerializedName("statenotes")
	val statenotes: String?,

	@Expose
	@field:SerializedName("recovered")
	val recovered: String?,

	@Expose
	@field:SerializedName("deltadeaths")
	val deltadeaths: String?,

	@Expose
	@field:SerializedName("migratedother")
	val migratedother: String?,

	@Expose
	@field:SerializedName("deltarecovered")
	val deltarecovered: String?,

	@Expose
	@field:SerializedName("active")
	val active: String?,

	@Expose
	@field:SerializedName("deltaconfirmed")
	val deltaconfirmed: String?,

	@Expose
	@field:SerializedName("state")
	val state: String?,

	@Expose
	@field:SerializedName("statecode")
	val statecode: String?,

	@Expose
	@field:SerializedName("confirmed")
	val confirmed: String?,

	@Expose
	@field:SerializedName("deaths")
	val deaths: String?,

	@Expose
	@field:SerializedName("lastupdatedtime")
	val lastupdatedtime: String?
)

@Entity
data class TestedItem(

	@Expose
	@field:SerializedName("source3")
	val source3: String?,

	@Expose
	@field:SerializedName("totaldosesadministered")
	val totaldosesadministered: String?,

	@Expose
	@field:SerializedName("source4")
	val source4: String?,

	@Expose
	@field:SerializedName("positivecasesfromsamplesreported")
	val positivecasesfromsamplesreported: String?,

	@Expose
	@field:SerializedName("healthcareworkersvaccinated2nddose")
	val healthcareworkersvaccinated2nddose: String?,

	@Expose
	@field:SerializedName("over60years1stdose")
	val over60years1stdose: String?,

	@Expose
	@field:SerializedName("registrationflwhcw")
	val registrationflwhcw: String?,

	@Expose
	@field:SerializedName("samplereportedtoday")
	val samplereportedtoday: String?,

	@Expose
	@field:SerializedName("registrationabove45years")
	val registrationabove45years: String?,

	@Expose
	@field:SerializedName("source")
	val source: String?,

	@Expose
	@field:SerializedName("source2")
	val source2: String?,

	@Expose
	@field:SerializedName("totalrtpcrsamplescollectedicmrapplication")
	val totalrtpcrsamplescollectedicmrapplication: String?,

	@Expose
	@field:SerializedName("registrationonline")
	val registrationonline: String?,

	@Expose
	@field:SerializedName("frontlineworkersvaccinated1stdose")
	val frontlineworkersvaccinated1stdose: String?,

	@Expose
	@field:SerializedName("over60years2nddose")
	val over60years2nddose: String?,

	@Expose
	@field:SerializedName("testsconductedbyprivatelabs")
	val testsconductedbyprivatelabs: String?,

	@Expose
	@field:SerializedName("healthcareworkersvaccinated1stdose")
	val healthcareworkersvaccinated1stdose: String?,

	@Expose
	@field:SerializedName("to60yearswithco-morbidities1stdose")
	val to60yearswithcoMorbidities1stdose: String?,

	@Expose
	@field:SerializedName("dailyrtpcrsamplescollectedicmrapplication")
	val dailyrtpcrsamplescollectedicmrapplication: String?,

	@Expose
	@field:SerializedName("totaldosesavailablewithstates")
	val totaldosesavailablewithstates: String?,

	@Expose
	@field:SerializedName("totalsessionsconducted")
	val totalsessionsconducted: String?,

	@Expose
	@field:SerializedName("totaldosesavailablewithstatesprivatehospitals")
	val totaldosesavailablewithstatesprivatehospitals: String?,

	@Expose
	@field:SerializedName("updatetimestamp")
	val updatetimestamp: String?,

	@Expose
	@field:SerializedName("totalpositivecases")
	val totalpositivecases: String?,

	@Expose
	@field:SerializedName("registrationonspot")
	val registrationonspot: String?,

	@Expose
	@field:SerializedName("years1stdose")
	val years1stdose: String?,

	@Expose
	@field:SerializedName("seconddoseadministered")
	val seconddoseadministered: String?,

	@Expose
	@field:SerializedName("totalsamplestested")
	val totalsamplestested: String?,

	@Expose
	@field:SerializedName("totaldosesinpipeline")
	val totaldosesinpipeline: String?,

	@Expose
	@field:SerializedName("totalindividualsregistered")
	val totalindividualsregistered: String?,

	@Expose
	@field:SerializedName("frontlineworkersvaccinated2nddose")
	val frontlineworkersvaccinated2nddose: String?,

	@Expose
	@field:SerializedName("firstdoseadministered")
	val firstdoseadministered: String?,

	@Expose
	@field:SerializedName("years2nddose")
	val years2nddose: String?,

	@Expose
	@field:SerializedName("over45years1stdose")
	val over45years1stdose: String?,

	@Expose
	@field:SerializedName("totalvaccineconsumptionincludingwastage")
	val totalvaccineconsumptionincludingwastage: String?,

	@Expose
	@field:SerializedName("testedasof")
	val testedasof: String?,

	@Expose
	@field:SerializedName("totaldosesprovidedtostatesuts")
	val totaldosesprovidedtostatesuts: String?,

	@Expose
	@field:SerializedName("registration18-45years")
	val registration1845years: String?,

	@Expose
	@field:SerializedName("over45years2nddose")
	val over45years2nddose: String?,

	@Expose
	@field:SerializedName("to60yearswithco-morbidities2nddose")
	val to60yearswithcoMorbidities2nddose: String?,

	@Expose
	@field:SerializedName("totalindividualstested")
	val totalindividualstested: String?
)

@Entity
data class CasesTimeSeriesItem(

	@Expose
	@field:SerializedName("date")
	val date: String?,

	@Expose
	@field:SerializedName("dailyrecovered")
	val dailyrecovered: String?,

	@Expose
	@field:SerializedName("dateymd")
	val dateymd: String?,

	@Expose
	@field:SerializedName("totalconfirmed")
	val totalconfirmed: String?,

	@Expose
	@field:SerializedName("totaldeceased")
	val totaldeceased: String?,

	@Expose
	@field:SerializedName("dailydeceased")
	val dailydeceased: String?,

	@Expose
	@field:SerializedName("totalrecovered")
	val totalrecovered: String?,

	@Expose
	@field:SerializedName("dailyconfirmed")
	val dailyconfirmed: String?
)
