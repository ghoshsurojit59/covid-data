package `in`.example.coviddata.view

import `in`.example.coviddata.databinding.FragmentStatesBinding
import `in`.example.coviddata.viewmodel.DataViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class StatesFragment: Fragment() {

    private lateinit var mBinding: FragmentStatesBinding
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adapterClass: StateAdapterClass
    private lateinit var datalistViewModel: DataViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentStatesBinding.inflate(inflater,container,false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView(){
        datalistViewModel = ViewModelProvider(this).get(DataViewModel::class.java)
        layoutManager = LinearLayoutManager(context)
        mBinding.recycvStates.layoutManager = layoutManager
        getServerData()
    }

    private fun getServerData(){

        datalistViewModel.getCasesDataFromDB().observe(viewLifecycleOwner) {

            if (it.statewise!!.isEmpty()) {
                Toast.makeText(context, "No Data!", Toast.LENGTH_LONG).show()
            } else {

                adapterClass = StateAdapterClass(it.statewise)
                mBinding.recycvStates.adapter = adapterClass
            }
        }
    }
}