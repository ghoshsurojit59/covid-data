package `in`.example.coviddata.view

import `in`.example.coviddata.databinding.CasesListItemBinding
import `in`.example.coviddata.model.CasesTimeSeriesItem
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class CasesAdapterClass (private val list: List<CasesTimeSeriesItem?>?): RecyclerView.Adapter<CasesAdapterClass.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = CasesListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        with(holder){
            mBinding.textDate.text = list!![position]!!.date
            mBinding.textTotalConf.text = list[position]!!.totalconfirmed
            mBinding.textTotalDesc.text = list[position]!!.totaldeceased
            mBinding.textTotalRec.text = list[position]!!.totalrecovered

        }
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    inner class ViewHolder(val mBinding: CasesListItemBinding): RecyclerView.ViewHolder(mBinding.root)
}