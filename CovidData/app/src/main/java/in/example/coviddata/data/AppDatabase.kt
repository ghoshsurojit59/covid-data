package `in`.example.coviddata.data

import `in`.example.coviddata.model.CovidDataConverter
import `in`.example.coviddata.model.CovidDataResponse
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [CovidDataResponse::class], version = 1, exportSchema = false)
@TypeConverters(CovidDataConverter::class)
abstract class AppDatabase : RoomDatabase(){

    abstract fun covidDataDao(): CovidDataDao

    companion object{
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "roomdb")
                    .build()
            }
            return INSTANCE as AppDatabase
        }
    }

}