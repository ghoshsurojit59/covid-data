package `in`.example.coviddata.repository

import `in`.example.coviddata.data.AppDatabase
import `in`.example.coviddata.data.CovidDataDao
import `in`.example.coviddata.model.CovidDataResponse
import `in`.example.coviddata.network.ApiInstance
import `in`.example.coviddata.network.DisplayListService
import `in`.example.coviddata.network.NetworkStatus.Companion.networkAvailable
import android.app.Application
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CovidListRepository(private val app: Application) {

    val dataResponseData = MutableLiveData<CovidDataResponse>()
    private lateinit var userDao: CovidDataDao
    private lateinit var allDetails: CovidDataResponse
    private lateinit var userItem: CovidDataResponse
    private val db = AppDatabase.getInstance(app)

    init {

        CoroutineScope(Dispatchers.IO).launch {
            userDao = db.covidDataDao()
            if(networkAvailable(app)){
                callWebService()

            }else{
                allDetails = userDao.allDataItem()
                dataResponseData.postValue(allDetails)
            }
            /* if(check){
                 callWebService()
                 check = false
             }else if(!check){
                 allDetails = userDao.allDataItem()
                 dataResponseData.postValue(allDetails)
             }*/
            /*  if(allDetails.data!!.isEmpty()){
                  callWebService()
              }else{
                  dataResponseData.postValue(allDetails)
              }*/


        }
    }

    @WorkerThread
    suspend fun callWebService() {
        if (networkAvailable(app)) {
            val service = ApiInstance.getApiInstance().create(DisplayListService::class.java)
            try {
                userItem = service.getAllData()
                dataResponseData.postValue(userItem)
                userDao.insert(userItem)

                Log.e("Response ", userItem.toString())

            } catch (e: Exception) {
                e.message?.let { Log.e("Error ", it) }
            }


        }

    }
}