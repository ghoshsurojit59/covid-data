package `in`.example.coviddata.view

import `in`.example.coviddata.databinding.FragmentTestsBinding
import `in`.example.coviddata.viewmodel.DataViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class TestsFragment: Fragment() {

    private lateinit var mBinding: FragmentTestsBinding
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adapterClass: TestsAdapterClass
    private lateinit var datalistViewModel: DataViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentTestsBinding.inflate(inflater,container,false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView(){
        datalistViewModel = ViewModelProvider(this).get(DataViewModel::class.java)
        layoutManager = LinearLayoutManager(context)
        mBinding.recycvTest.layoutManager = layoutManager
        getServerData()
    }

    private fun getServerData(){

        datalistViewModel.getCasesDataFromDB().observe(viewLifecycleOwner) {

            if (it.tested!!.isEmpty()) {
                Toast.makeText(context, "No Data!", Toast.LENGTH_LONG).show()
            } else {

                adapterClass = TestsAdapterClass(it.tested)
                mBinding.recycvTest.adapter = adapterClass
            }
        }
    }
}