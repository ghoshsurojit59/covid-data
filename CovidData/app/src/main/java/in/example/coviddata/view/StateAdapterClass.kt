package `in`.example.coviddata.view

import `in`.example.coviddata.databinding.StatesListItemBinding
import `in`.example.coviddata.model.StatewiseItem
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class StateAdapterClass (private val list: List<StatewiseItem?>?): RecyclerView.Adapter<StateAdapterClass.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = StatesListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        with(holder){
            mBinding.textState.text = list!![position]!!.state
            mBinding.textActive.text = list[position]!!.active
            mBinding.textDeaths.text = list[position]!!.deaths
            mBinding.textRecovered.text = list[position]!!.recovered

        }
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    inner class ViewHolder(val mBinding: StatesListItemBinding): RecyclerView.ViewHolder(mBinding.root)
}