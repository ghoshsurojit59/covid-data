package `in`.example.coviddata.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class CovidDataConverter {

    private val gson = Gson()

    @TypeConverter
    fun fromCasesTimeSeriesItem(DetailsItem: List<CasesTimeSeriesItem?>?): String? {
        if (DetailsItem == null) {
            return null
        }
        val type: Type = object : TypeToken<List<CasesTimeSeriesItem?>?>() {}.type
        return gson.toJson(DetailsItem, type)
    }

    @TypeConverter
    fun toCasesTimeSeriesItem(DetailsItem: String?): List<CasesTimeSeriesItem?>? {
        if (DetailsItem == null) {
            return null
        }
        val type: Type = object : TypeToken<List<CasesTimeSeriesItem?>?>() {}.type
        return gson.fromJson<List<CasesTimeSeriesItem?>>(DetailsItem, type)
    }

    @TypeConverter
    fun fromTestedItem(DetailsItem: List<TestedItem?>?): String? {
        if (DetailsItem == null) {
            return null
        }
        val type: Type = object : TypeToken<List<TestedItem?>?>() {}.type
        return gson.toJson(DetailsItem, type)
    }

    @TypeConverter
    fun toTestedItem(DetailsItem: String?): List<TestedItem?>? {
        if (DetailsItem == null) {
            return null
        }
        val type: Type = object : TypeToken<List<TestedItem?>?>() {}.type
        return gson.fromJson<List<TestedItem?>>(DetailsItem, type)
    }

    @TypeConverter
    fun fromStatewiseItem(DetailsItem: List<StatewiseItem?>?): String? {
        if (DetailsItem == null) {
            return null
        }
        val type: Type = object : TypeToken<List<StatewiseItem?>?>() {}.type
        return gson.toJson(DetailsItem, type)
    }

    @TypeConverter
    fun toStatewiseItem(DetailsItem: String?): List<StatewiseItem?>? {
        if (DetailsItem == null) {
            return null
        }
        val type: Type = object : TypeToken<List<StatewiseItem?>?>() {}.type
        return gson.fromJson<List<StatewiseItem?>>(DetailsItem, type)
    }
}