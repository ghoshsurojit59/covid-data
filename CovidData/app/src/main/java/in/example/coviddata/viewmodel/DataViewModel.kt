package `in`.example.coviddata.viewmodel

import `in`.example.coviddata.model.CovidDataResponse
import `in`.example.coviddata.repository.CovidListRepository
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData

class DataViewModel (app: Application) : AndroidViewModel(app) {

    var getCasesDataObserver : MediatorLiveData<CovidDataResponse> = MediatorLiveData()
    private val dataRepo = CovidListRepository(app)

    fun getCasesDataFromDB(): MediatorLiveData<CovidDataResponse> {
        getCasesDataObserver.removeSource(dataRepo.dataResponseData)
        getCasesDataObserver.addSource(dataRepo.dataResponseData, getCasesDataObserver::setValue)
        return getCasesDataObserver
    }

}