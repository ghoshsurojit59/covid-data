package `in`.example.coviddata.network

import `in`.example.coviddata.model.CovidDataResponse
import retrofit2.http.GET
import retrofit2.http.Headers

interface DisplayListService {

    @GET("data.json")
    @Headers("Accept:application/json", "Content-Type:application/json")
    suspend fun getAllData(): CovidDataResponse

}