package `in`.example.coviddata.view

import `in`.example.coviddata.databinding.TestsListItemBinding
import `in`.example.coviddata.model.TestedItem
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class TestsAdapterClass (private val list: List<TestedItem?>?): RecyclerView.Adapter<TestsAdapterClass.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding = TestsListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        with(holder){
            mBinding.textTestAsOf.text = list!![position]!!.testedasof
            mBinding.textSampleColl.text = list[position]!!.dailyrtpcrsamplescollectedicmrapplication
            mBinding.textSampleReport.text = list[position]!!.samplereportedtoday
            mBinding.textTotalDose.text = list[position]!!.totaldosesadministered

        }
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    inner class ViewHolder(val mBinding: TestsListItemBinding): RecyclerView.ViewHolder(mBinding.root)
}