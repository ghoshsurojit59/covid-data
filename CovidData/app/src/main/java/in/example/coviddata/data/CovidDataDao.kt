package `in`.example.coviddata.data

import `in`.example.coviddata.model.CovidDataResponse
import androidx.room.*

@Dao
interface CovidDataDao {

    @Query("SELECT * FROM CovidDataResponse")
    fun allDataItem(): CovidDataResponse

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userItem: CovidDataResponse)

    @Update
    fun update(userItem: CovidDataResponse)

    @Delete
    fun delete(userItem: CovidDataResponse)

    /* @Query("delete from ServerDetailsResponse")
     fun deleteAllNotes()*/
}